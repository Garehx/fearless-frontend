function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
            <div class="card" style="margin-bottom: 20px;">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    <p class="card-text">${starts} - ${ends}</p>
                </div>
            </div>
        `;
}

function alertError(){
    return`
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Holy #$@!</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        let currentColumn = 0;

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts).toLocaleDateString()
            const ends = new Date(details.conference.ends).toLocaleDateString()
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, starts, ends, location);
            console.log(html);
            const column = document.querySelector(`#col-${currentColumn}`)
            column.innerHTML += html
            currentColumn ++;
            if (currentColumn > 2) {
                currentColumn = 0
            }
          }
        }

      }
    } catch (e) {
      const err = alertError();
      const header = document.querySelector('header');
      header.outerHTML += err;
    }

  });