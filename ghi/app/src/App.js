import Nav from './Nav';
import AttendeesList from './AttendeesList';
import React from "react";
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendeeConferenceForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
      <div className="container">
          {/* <ConferenceForm /> */}
          {/* <LocationForm /> */}
          <AttendConferenceForm />
          {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </>
  );
}
export default App;